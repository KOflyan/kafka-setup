# kafka-setup

Ready-to-go zookeeper + kafka bundle.

### Prerequisites:

* Docker
* Kubernetes
* Helm

### Setup

1) Navigate to `.deploy/` folder
2) Run `helm install kafka-app .` *
3) Verify installation `kubectl get pods` (all should have `running` status)

*If running first time, it will take some time to install (around 5 minutes, but it depends on your machine and internet speed)


kafka ui - http://localhost:3001

kafka    - http://localhost:3000